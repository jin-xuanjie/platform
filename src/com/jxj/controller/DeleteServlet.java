package com.jxj.controller;

import com.jxj.dao.UserDao;
import com.jxj.util.ResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@MultipartConfig//文件上传
@WebServlet("/deletearticle")
public class DeleteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

    String  id = request.getParameter("id");
        int count=UserDao.delete(id);
        PrintWriter out=response.getWriter();

        if (count==1){
            String result= ResponseUtil.resp(200,null ,"删除成功");
            out.print(result);
        }
        else {
            String result= ResponseUtil.resp(400,null ,"删除失败");
            out.print(result);
        }

    }


}
