package com.jxj.controller;

import com.jxj.dao.GameDaoImp;
import com.jxj.dao.WorkDao;
import com.jxj.entity.Work;
import com.jxj.entity.getResult;
import com.jxj.util.ReflectUtil;
import com.jxj.util.ResponseUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.lang.String;
import java.util.List;

/**
 * @author Ricky
 * @create 2021/2/18 16:19
 */
@MultipartConfig//文件上传
@WebServlet("/fourpro")

public class FourProServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json");

        PrintWriter pw = resp.getWriter();
        List<Work> list=WorkDao.getFourPro();
        getResult gr = null;

        gr = new getResult(200,list,"success");

        String json = gr.toJSON();
        System.out.println(json);
        resp.getWriter().append(json);


    }
}
