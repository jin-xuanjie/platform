package com.jxj.controller;



import com.jxj.entity.Game;
import com.jxj.service.DBService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import com.jxj.entity.Result;
import java.io.IOException;
@MultipartConfig//发布比赛
@WebServlet("/releasegame")
public class GameServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String gName = request.getParameter("gName");
        String gIntro = request.getParameter("gIntroduction");
        Part part= request.getPart("gCImg");
        String fileName =null;
        if (part!=null){
           fileName = part.getSubmittedFileName();
        }
        String realPath1 = null;
        if (fileName !=null&&!"".equals(fileName)){
            realPath1=request.getServletContext().getRealPath("/upload/");
//            System.out.println(realPath1);
            part.write(realPath1+fileName);

        }

        String gCImg = realPath1 + fileName;
        String s=gCImg.substring(24);
        String s1="http://123.57.207.22:8080"+s;
        Game game = new Game(gName,gIntro,s1);
        int gameId = DBService.insertGame(game);

        Result r = null;
        if(gameId == 1){

            r = new Result(200,"发布成功");
            RequestDispatcher report =request.getRequestDispatcher("/success.html");
            report.forward(request,response);

        }else {
            r = new Result(400,"发布失败");
        }
        String json = r.toJSON();
        response.getWriter().append(json);
    }
}
