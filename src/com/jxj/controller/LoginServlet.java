package com.jxj.controller;

import com.jxj.dao.UserDao;
import com.jxj.entity.Login;
import com.jxj.util.ReflectUtil;
import com.jxj.util.ResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@MultipartConfig
@WebServlet("/login")

public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String studyNo,password;
        studyNo =request.getParameter("studyNo");
        password =request.getParameter("password");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
            PrintWriter out=response.getWriter();
            String msg ="登陆失败";
            if (UserDao.login(studyNo,password)){
                msg ="登陆成功";
                String admin=String.valueOf(UserDao.findAdmin(studyNo));
                String uId=String.valueOf(UserDao.findUId(studyNo));
                Integer adm=Integer.parseInt(admin);
                Integer u=Integer.parseInt(uId);
                Login login=new Login(adm,u);
                String log=ReflectUtil.jsonObject(login);
                String result= ResponseUtil.resp(200,log,msg);
                out.print(result);
                Cookie card1=new Cookie("studyNo",studyNo);
                Cookie card2=new Cookie("admin", admin);
                Cookie card3=new Cookie("uId", uId);


                response.addCookie(card1);
                response.addCookie(card2);
                response.addCookie(card3);

    } else{
            msg ="账号密码错误或还没有注册呦";
            String result= ResponseUtil.resp(400,null ,msg);
            out.print(result);
        }
    }

}
