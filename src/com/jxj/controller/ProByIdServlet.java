package com.jxj.controller;

import com.jxj.dao.WorkDao;
import com.jxj.entity.Work;
import com.jxj.util.ReflectUtil;
import com.jxj.util.ResponseUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.lang.String;

/**
 * @author Ricky
 * @create 2021/2/18 16:19
 */
@MultipartConfig//文件上传
@WebServlet("/getproductsbyid")

public class ProByIdServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WorkDao workDao = new WorkDao();
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json");

        PrintWriter pw = resp.getWriter();

        String proId = req.getParameter("id");
        //String msg = "获取失败";

        try {

            Work work = workDao.getProductsById(Integer.parseInt(proId));

            if (work != null) {
                String a= ReflectUtil.jsonObject(work);
                pw.print(ResponseUtil.resp(200,a,"获取成功"));
            }
            else {
                pw.print(ResponseUtil.resp(400,null,"获取失败"));

            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }

    }
}
