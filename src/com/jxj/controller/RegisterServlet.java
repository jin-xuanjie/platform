package com.jxj.controller;

import com.jxj.dao.UserDao;
import com.jxj.entity.Login;
import com.jxj.entity.Users;
import com.jxj.util.ReflectUtil;
import com.jxj.util.ResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/register")
@MultipartConfig//文件上传
public class RegisterServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String name,studyNo,college,password;
        name =request.getParameter("name");
        studyNo =request.getParameter("studyNo");
        college =request.getParameter("college");
        password =request.getParameter("password");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out=response.getWriter();
        String msg ="注册失败";
        if (UserDao.isExist(studyNo)){
        msg ="您已注册过了";

        }
        if (studyNo.length()!=10){
            String result= ResponseUtil.resp(400,null ,"您的学号不合法");
            out.print(result);
            return;        }
        if (password.length()<6){
            String result= ResponseUtil.resp(400,null ,"您的密码太短，有点危险呀");
            out.print(result);
            return;        }
        Users users =new Users(null,name,studyNo,college,password);
        int count=UserDao.add(users);

        if (count==1){
            msg="注册成功";

            Integer admin=UserDao.findAdmin(studyNo);
            String uId=String.valueOf(UserDao.findUId(studyNo));
            Integer u=Integer.parseInt(uId);
           String adm=String.valueOf(admin);
            Login login=new Login(admin,u);
            String log=ReflectUtil.jsonObject(login);

            String result= ResponseUtil.resp(200,log ,msg);
            out.print(result);
            Cookie card1=new Cookie("studyNo",studyNo);
            Cookie card2=new Cookie("admin", adm);
            response.addCookie(card1);
            response.addCookie(card2);


        }else {
            out.print( ResponseUtil.resp(400,null,msg));
        }




    }
}

