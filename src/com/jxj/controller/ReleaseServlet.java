package com.jxj.controller;

import com.jxj.dao.ProDao;
import com.jxj.dao.UserDao;
import com.jxj.entity.Pros;
import com.jxj.util.ResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@MultipartConfig//文件上传
@WebServlet("/addarticle")

public class ReleaseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");

//        Cookie cookieArray[] = request.getCookies();
        String key,value;
        String name,studyNo = null,proName,introduction,admin = null;
        String msg ="发布失败";
        PrintWriter out=response.getWriter();
//if (cookieArray[0]==null){
//    msg="请您先登录";
//    String result= ResponseUtil.resp(400,null ,msg);
//    out.print(result);
//    return;
//}
//        for (Cookie a : cookieArray
//        ) {
//            key = a.getName();
//            value = a.getValue();
//            if ("studyNo".equals(key)) {
//                studyNo = value;
//            } else if ("admin".equals(key)) {
//                admin = value;
//            }
//        }
//        if ("".equals(studyNo)||"null".equals(studyNo)){
//            msg="请您先登录";
//            String result= ResponseUtil.resp(400,null ,msg);
//            out.print(result);
//            return;
//        }
//        if ("0".equals(admin)){
//            msg="您还不是管理员";
//            String result= ResponseUtil.resp(400,null ,msg);
//            out.print(result);
//            return;
//
//        }
        String realPath1 = null;
        String realPath2 = null;
        String fileName=null;
        String fileName1=null;

        name=request.getParameter("name");
        studyNo=request.getParameter("studyNo");
        introduction=request.getParameter("introduction");
        proName=request.getParameter("proName");
        if (!UserDao.isPair(name,studyNo)){
            msg ="名字和学号不匹配";
            String result= ResponseUtil.resp(400,null ,msg);
            out.print(result);
            return;
        }
        Integer uId =UserDao.findUId(studyNo);
        Part part= request.getPart("cover");
        Part part1=request.getPart("content");
       if (part!=null){
         fileName=part.getSubmittedFileName();}
       if  (part1!=null){
           fileName1=part1.getSubmittedFileName();}
        if (fileName !=null&&!"".equals(fileName)){
             realPath1=request.getServletContext().getRealPath("/upload/");
            System.out.println(realPath1);
            part.write(realPath1+fileName);

        }
        if (fileName1 !=null&&!"".equals(fileName1)){
             realPath2=request.getServletContext().getRealPath("/upload/");
            System.out.println(realPath2);
            System.out.println(realPath2+fileName1);
            part1.write(realPath2+fileName1);
        }
        String s=realPath1.substring(24);
        String s1=realPath2.substring(24);
        String s2="http://123.57.207.22:8080"+s;
        String s3="http://123.57.207.22:8080"+s1;

        Pros pro=new Pros(null,proName,introduction,(s2+fileName),(s3+fileName1),uId);
        int count = ProDao.add(pro);
        if (count ==1){
            msg="发布成功";
            out.print(ResponseUtil.resp(200,null,msg));
      request.getRequestDispatcher("/success.html").forward(request, response);
        }else{

            out.print(ResponseUtil.resp(400,null,msg));


        } }


}
