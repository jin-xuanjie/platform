package com.jxj.controller;

import com.jxj.dao.UserDao;
import com.jxj.util.ResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@MultipartConfig
@WebServlet("/updatepassword")

public class UpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studyNo = null, admin, password, newPassword, key = null, value = null;
        request.setCharacterEncoding("utf-8");
        password = request.getParameter("password");
        newPassword = request.getParameter("newPassword");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String msg = "修改失败";
        if (password.equals(newPassword)){
            msg = "与原密码相同";
            out.print( ResponseUtil.resp(400,null,msg));
            return;
        }


        Cookie cookieArray[] = request.getCookies();
        for (Cookie a : cookieArray
        ) {
            key = a.getName();
            value = a.getValue();
            if ("studyNo".equals(key)) {
                studyNo = value;
            } else if ("admin".equals(key)) {
                admin = value;
            }
        }

        if(UserDao.login(studyNo,password)){
            int count=UserDao.update(newPassword,studyNo);
            if (count == 1){
                msg ="修改成功";
                out.print( ResponseUtil.resp(200,null,msg));

            }else {
                out.print( ResponseUtil.resp(400,null,msg));
            }


        }else {
            msg ="原密码错误";
            out.print( ResponseUtil.resp(400,null,msg));

        }

    }
}
