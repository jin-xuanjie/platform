package com.jxj.controller;



import com.jxj.dao.GameDaoImp;
import com.jxj.entity.getResult;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

//获取六个比赛作品
@WebServlet("/getcatch")
public class getGameServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/json;charset=utf-8");

        getResult gr = null;

        gr = new getResult(200, GameDaoImp.getFourPro(),"success");

        String json = gr.toJSON();
        response.getWriter().append(json);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
