package com.jxj.dao;

public interface BaseDao<T> {
    /**
     * 用于规范操作数据库表格的（新增）数据规则
     * @return 返回的是新增结果，为一个数字，大于0表示新增成功
     */
    int insert(T t);


}
