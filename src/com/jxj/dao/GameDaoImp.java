package com.jxj.dao;


import com.jxj.entity.Game;
import com.jxj.util.DButil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameDaoImp implements BaseDao<Game>{


    private static final String SQL_INSERT = "insert into t_game(gName,gIntro,gCImg) values(?,?,?) ";


//发布比赛
    @Override
    public int insert(Game o) {
        Connection conn = DButil.getConn();
        PreparedStatement state = null;
        int count=0;
        try {

            state = conn.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);


            state.setString(1,o.gName);
            state.setString(2,o.gIntro);
            state.setString(3,o.gCImg);

            count=state.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            DButil.close(conn,state,null);
        }

        return count;
    }

//获取比赛信息

    public static List<Game> getFourPro() {
        List<Game> g = new ArrayList<>();
        String sql = "select * from t_game order by gId desc limit 0,6";

        Statement st = null;
        ResultSet resultSet = null;
        Connection con = null;

        try {

            con = DButil.getConn();
            st = con.createStatement();
            resultSet = st.executeQuery(sql);

            Game game = null;

            while (resultSet.next()) {
                int gId = resultSet.getInt("gId");
                String gName = resultSet.getString("gName");
                String gIntro = resultSet.getString("gIntro");
                String gCImg= resultSet.getString("gCImg");


                game = new Game();

                game.setgId(gId);
                game.setgName(gName);
                game.setgIntro(gIntro);
                game.setgCImg(gCImg);

                g.add(game);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return g;
    }
}
