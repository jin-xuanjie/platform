package com.jxj.dao;

import com.jxj.entity.Pros;
import com.jxj.entity.Users;
import com.jxj.util.JdbcUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProDao {
    public  static int add(Pros pro){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        int count = 0;
        try {
            conn = JdbcUtil.getConnection();
            String sql ="insert into t_pro(proName,proIntroduction,cover,proContent,uId) values(?,?,?,?,?)";
            ps=conn.prepareStatement(sql);
            ps.setString(1,pro.getProName());
            ps.setString(2,pro.getProIntroduction());
            ps.setString(3,pro.getImg());
            ps.setString(4,pro.getFile1());
            ps.setString(5,String.valueOf(pro.getuId()));

            count=  ps.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return count;
    }
}
