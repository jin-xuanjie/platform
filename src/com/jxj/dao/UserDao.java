package com.jxj.dao;

import com.jxj.entity.Users;
import com.jxj.entity.Work;
import com.jxj.util.JDBCUtil1;
import com.jxj.util.JdbcUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    public  static int add(Users users){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        int count = 0;
        try {
            conn = JdbcUtil.getConnection();
            String sql ="insert into t_user(name,password,college,studyNo) values(?,?,?,?)";
            ps=conn.prepareStatement(sql);
            ps.setString(1,users.getName());
            ps.setString(2,users.getPassword());
            ps.setString(3,users.getCollege());
            ps.setString(4,users.getStudyNo());

            count=  ps.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return count;
    }
    public  static boolean isExist(String studyNo){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        List<Users> userList =  new ArrayList<>();
        try {

            conn =JdbcUtil.getConnection();
            String sql ="select * from t_user where studyNo ="+studyNo;
            ps=conn.prepareStatement(sql);

            rs=  ps.executeQuery();
            if (rs.next()){
                return true;
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return false;
    }
    public  static Integer findAdmin(String studyNo){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        Integer admin =null;
        try {

            conn =JdbcUtil.getConnection();
            String sql ="select admin from t_user where studyNo ="+studyNo;
            ps=conn.prepareStatement(sql);

            rs=  ps.executeQuery();
            if (rs.next()){
                 admin=rs.getInt("admin");
            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return admin;
    }
    public  static boolean login(String studyNo,String password){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        try {

            conn =JdbcUtil.getConnection();
            String sql ="select * from t_user where studyNo ="+studyNo+" and password ="+password;
            ps=conn.prepareStatement(sql);

            rs=  ps.executeQuery();
            if (rs.next()){
                return true;
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return false;
    }
    public  static Integer update(String newpassword,String studyNo){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        int count = 0;
        try {
            conn = JdbcUtil.getConnection();
            String sql ="update t_user set password = ? where studyNo =? ";
            ps=conn.prepareStatement(sql);
            ps.setString(1,newpassword);
            ps.setString(2,studyNo);

            count=  ps.executeUpdate();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return count;
    }
    public  static Integer findUId(String studyNo){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        Integer admin =null;
        try {

            conn =JdbcUtil.getConnection();
            String sql ="select uId from t_user where studyNo ="+studyNo;
            ps=conn.prepareStatement(sql);

            rs=  ps.executeQuery();
            if (rs.next()){
                admin=rs.getInt("uId");
            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return admin;
    }

    public  static boolean isPair(String name,String studyNo){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        try {

            conn =JdbcUtil.getConnection();
            String sql ="select * from t_user where studyNo = ? and name = ?";
            ps=conn.prepareStatement(sql);
            ps.setString(1,studyNo);
            ps.setString(2,name);

            rs=  ps.executeQuery();
            if (rs.next()){
                return true;
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return false;
    }
    public  static Integer delete(String id){
        Connection conn =null;
        PreparedStatement ps =null;
        ResultSet rs = null;
        int count =0;
        try {

            conn =JdbcUtil.getConnection();
            String sql ="delete from t_pro where proId =?";
            ps=conn.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(id));


             count=ps.executeUpdate();


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtil.close(conn,ps,rs);
        }
        return count;
    }





    //获取最热的4个作品
    //public static List<Work> getFourPro() {
}

