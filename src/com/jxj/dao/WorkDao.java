package com.jxj.dao;


import com.jxj.entity.Work;
import com.jxj.util.JDBCUtil1;
import com.jxj.util.JdbcUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ricky
 * @create 2021/2/17 11:13
 */
public class WorkDao {

    //获取最热的4个作品
    //public static List<Work> getFourPro() {
    public static List<Work> getFourPro() {
        //List<Work> works = new ArrayList<>();
        List<Work> list =new ArrayList<>();
        Work work = null;
        String sql = "select * from t_pro p join t_user u on  p.uId=u.uId  order by lNumber desc";
        //List newList = works.subList(0,4);
        int index=0;
        Statement st = null;
        ResultSet rs = null;
        Connection con = null;

        try {

            con = JDBCUtil1.getConnection();
            st = con.createStatement();
            rs = st.executeQuery(sql);

            //Work work = null;
            while (rs.next()) {
                int proId = rs.getInt("proId");
                String proName = rs.getString("proName");
                String proIntroduction = rs.getString("proIntroduction");
                int uId = rs.getInt("uId");
                String name= rs.getString("name");
                String studyNo = rs.getString("studyNo");
                String cover = rs.getString("cover");
                String proContent = rs.getString("proContent");
                String lNumber = rs.getString("lNumber");

                work = new Work();

                work.setProId(proId);
                work.setProName(proName);
                work.setProIntroduction(proIntroduction);
                work.setuId(uId);
                work.setName(name);
                work.setStudyNo(studyNo);
                work.setCover(cover);
                work.setProContent(proContent);
                work.setlNumber(lNumber);
                list.add(work);
                if (++index>=4){
                    break;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (st != null) {
                try {
                    con.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

        return list;
    }

    //按id
    public Work getProductsById(int Id) {
        //List<Work> works = new ArrayList<>();
        Work work = null;
        String sql = "select * from t_pro p join t_user u on  p.uId=u.uId where proId = ?";
        Connection con = null;
        PreparedStatement ps =null;
        ResultSet rs = null;


        try {

            con = JdbcUtil.getConnection();
            ps=con.prepareStatement(sql);
        ps.setInt(1,Id);
            //Work work = null;
            rs=ps.executeQuery();
            while (rs.next()) {
                int proId = rs.getInt("proId");
                String proName = rs.getString("proName");
                String proIntroduction = rs.getString("proIntroduction");
                int uId = rs.getInt("uId");
                String name= rs.getString("name");
                String studyNo = rs.getString("studyNo");
                String cover = rs.getString("cover");
                String proContent = rs.getString("proContent");
                String lNumber = rs.getString("lNumber");

                work = new Work();

                work.setProId(proId);
                work.setProName(proName);
                work.setProIntroduction(proIntroduction);
                work.setuId(uId);
                work.setName(name);
                work.setStudyNo(studyNo);
                work.setCover(cover);
                work.setProContent(proContent);
                work.setlNumber(lNumber);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (ps != null) {
                try {
                    con.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

        return work;
    }

    //按热度
    //public static List<Work> getAllArticlesByHot() {
    public static List<Work> getAllArticlesByHot() {
        List<Work> list = new ArrayList<>();
        Work work = null;
        String sql = "select * from t_pro p join t_user u on p.uId=u.uId order by lNumber desc;";

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;

        try {

            con = JdbcUtil .getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            while (rs.next()) {
                int proId = rs.getInt("proId");
                String proName = rs.getString("proName");
                String proIntroduction = rs.getString("proIntroduction");
                int uId = rs.getInt("uId");
                String name= rs.getString("name");
                String studyNo = rs.getString("studyNo");
                String cover = rs.getString("cover");
                String proContent = rs.getString("proContent");
                String lNumber = rs.getString("lNumber");

                work = new Work();

                work.setProId(proId);
                work.setProName(proName);
                work.setProIntroduction(proIntroduction);
                work.setuId(uId);
                work.setName(name);
                work.setStudyNo(studyNo);
                work.setCover(cover);
                work.setProContent(proContent);
                work.setlNumber(lNumber);

                list.add(work);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (ps != null) {
                try {
                    con.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

        return list;
    }


    //按时间（数据库倒序）
    //public static List<Work> getAllArticlesByTime() {
     public static List<Work> getAllArticlesByTime() {
        List<Work> list = new ArrayList<>();
        Work work = null;
        String sql = "select * from t_pro p join t_user u on p.uId=u.uId order by proId desc;";

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;

        try {

            con = JdbcUtil .getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            while (rs.next()) {
                int proId = rs.getInt("proId");
                String proName = rs.getString("proName");
                String proIntroduction = rs.getString("proIntroduction");
                int uId = rs.getInt("uId");
                String name= rs.getString("name");
                String studyNo = rs.getString("studyNo");
                String cover = rs.getString("cover");
                String proContent = rs.getString("proContent");
                String lNumber = rs.getString("lNumber");

                work = new Work();

                work.setProId(proId);
                work.setProName(proName);
                work.setProIntroduction(proIntroduction);
                work.setuId(uId);
                work.setName(name);
                work.setStudyNo(studyNo);
                work.setCover(cover);
                work.setProContent(proContent);
                work.setlNumber(lNumber);

                list.add(work);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (ps != null) {
                try {
                    con.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

        return list;
    }

}
