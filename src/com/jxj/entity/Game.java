package com.jxj.entity;

import java.util.Objects;

public class Game {
    public int gId;
    public String gName;
    public String gIntro;
    public String gCImg;

    @Override
    public String toString() {
        return "game{" +
                "gId=" + gId +
                ", gName='" + gName + '\'' +
                ", gIntro='" + gIntro + '\'' +
                ", gCImg='" + gCImg + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return gId == game.gId && gName.equals(game.gName) &&  gIntro.equals(game.gIntro) && gCImg.equals(game.gCImg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gId, gName, gIntro, gCImg);
    }

    public int getgId() {
        return gId;
    }

    public void setgId(int gId) {
        this.gId = gId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getgIntro() {
        return gIntro;
    }

    public void setgIntro(String gIntro) {
        this.gIntro = gIntro;
    }

    public String getgCImg() {
        return gCImg;
    }

    public void setgCImg(String gCImg) {
        this.gCImg = gCImg;
    }

    public Game() {
    }

    public Game(String gName, String gIntro, String gCImg) {
        this.gName = gName;
        this.gIntro = gIntro;
        this.gCImg = gCImg;
    }

    public Game(int gId, String gName, String gIntro, String gCImg) {
        this.gId = gId;
        this.gName = gName;
        this.gIntro = gIntro;
        this.gCImg = gCImg;
    }

}
