package com.jxj.entity;

public class Games {
    private Integer gId;
    private String gName;
    private String college;
    private String gCImg;
    private String rTime;

    public Integer getgId() {
        return gId;
    }

    public void setgId(Integer gId) {
        this.gId = gId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getgCImg() {
        return gCImg;
    }

    public void setgCImg(String gCImg) {
        this.gCImg = gCImg;
    }

    public String getrTime() {
        return rTime;
    }

    public void setrTime(String rTime) {
        this.rTime = rTime;
    }

    public Games(Integer gId, String gName, String college, String gCImg, String rTime) {
        this.gId = gId;
        this.gName = gName;
        this.college = college;
        this.gCImg = gCImg;
        this.rTime = rTime;
    }

    public Games() {
    }
}