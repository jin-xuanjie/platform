package com.jxj.entity;

public class Login {
    private int admin;
    private int uId;

    public Login(int admin, int uId) {
        this.admin = admin;
        this.uId = uId;
    }

    public Login(int admin, int uId, String studyNo) {
        this.admin = admin;
        this.uId = uId;
    }
    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public Login() {
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }




}
