package com.jxj.entity;

public class Pros {
    private Integer proId;
    private String proName;
    private String proIntroduction;
    private String img;
    private String file1;
    private Integer uId;

    public Pros() {
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProIntroduction() {
        return proIntroduction;
    }

    public void setProIntroduction(String proIntroduction) {
        this.proIntroduction = proIntroduction;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getFile1() {
        return file1;
    }

    public void setFile1(String file1) {
        this.file1 = file1;
    }


    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Pros(Integer proId, String proName, String proIntroduction, String img, String file1, Integer uId) {
        this.proId = proId;
        this.proName = proName;
        this.proIntroduction = proIntroduction;
        this.img = img;
        this.file1 = file1;
        this.uId = uId;
    }
}