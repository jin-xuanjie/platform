package com.jxj.entity;

public class Users {
    private Integer uId;
    private String name;
    private String studyNo;
    private String college;
    private String password;

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudyNo() {
        return studyNo;
    }

    public void setStudyNo(String studyNo) {
        this.studyNo = studyNo;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Users(Integer uId, String name, String studyNo, String college, String password) {
        this.uId = uId;
        this.name = name;
        this.studyNo = studyNo;
        this.college = college;
        this.password = password;
    }

    public Users() {
    }
}