package com.jxj.entity;

import com.alibaba.fastjson.JSON;

/**
 * @author Ricky
 * @create 2021/2/17 11:13
 */
public class Work {

    private Integer proId;                  //作品id
    private String proName;             //作品名称
    private String proIntroduction;     //作品简介
    private int uId;                    //用户id
    private String name;                //姓名
    private String studyNo;             //学号
    private String cover;               //封面
    private String proContent;          //作品内容
    private String lNumber;             //点赞数

    public Work() {

    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProIntroduction() {
        return proIntroduction;
    }

    public void setProIntroduction(String proIntroduction) {
        this.proIntroduction = proIntroduction;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudyNo() {
        return studyNo;
    }

    public void setStudyNo(String studyNo) {
        this.studyNo = studyNo;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getProContent() {
        return proContent;
    }

    public void setProContent(String proContent) {
        this.proContent = proContent;
    }

    public String getlNumber() {
        return lNumber;
    }

    public void setlNumber(String lNumber) {
        this.lNumber = lNumber;
    }

    @Override
    public String toString() {
        return "Work{" +
                "proId=" + proId +
                ", proName='" + proName + '\'' +
                ", proIntroduction='" + proIntroduction + '\'' +
                ", uId=" + uId +
                ", name='" + name + '\'' +
                ", studyNo='" + studyNo + '\'' +
                ", cover='" + cover + '\'' +
                ", proContent='" + proContent + '\'' +
                ", lNumber='" + lNumber + '\'' +
                '}';
    }
    public String toJSON() {
        return JSON.toJSONString(this);

    }}