package com.jxj.entity;

import com.alibaba.fastjson.JSON;
//获取比赛信息result
public class getResult {
        private int code;
        private Object data;
        private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public getResult() {
    }

    public getResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public getResult(int code) {
        this.code = code;
    }

    public getResult(int code, Object data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }
    public String toJSON(){
        return JSON.toJSONString(this);
    }
}
