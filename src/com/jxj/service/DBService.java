package com.jxj.service;


import com.jxj.dao.BaseDao;
import com.jxj.dao.GameDaoImp;
import com.jxj.entity.Game;

//dao之前的的转承，无实际逻辑意义
public class DBService {
    private static BaseDao<Game> gameDao = new GameDaoImp();
    public static int insertGame(Game game){
        return gameDao.insert(game);
    }

}
