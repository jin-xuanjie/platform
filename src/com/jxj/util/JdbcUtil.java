package com.jxj.util;


import java.sql.*;

public class JdbcUtil{
    //为什么要用私有的构造方法呢？
    //为了防止你new对象。
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private JdbcUtil(){};
    public static Connection getConnection() throws ClassNotFoundException, SQLException {

        Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/zpfbpt","root","jxj123456");
        return conn;
    }
    public static void close(Connection conn, Statement ps, ResultSet rs){
        if (rs !=null){
            try {
                rs.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if (ps !=null){
            try {
                ps.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        if (conn !=null){
            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

}
