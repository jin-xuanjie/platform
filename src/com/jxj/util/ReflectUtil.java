package com.jxj.util;

import java.lang.reflect.Field;

public class ReflectUtil {
    public static String jsonObject(Object obj){
        Class classFile =null;
        Field fieldArray[] =null;
        StringBuffer str=new StringBuffer("{");
        //获得当前对象的class文件
        classFile =obj.getClass();
        //获得class文件的所有属性
        fieldArray=classFile.getDeclaredFields();
        //获取所有属性的值并且开始拼接
        for (int i=0;i<fieldArray.length;i++){
            Field field=fieldArray[i];
            //这里怕有private修饰的属性无法访问，特意弄了个访问权限
            field.setAccessible(true);
            String fieldName=field.getName();
            Object value = null;
            try {
                value = field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            str.append("\"");
            str.append(fieldName);
            str.append("\":");
            str.append("\"");
            str.append(value);
            str.append("\"");
            if (i<fieldArray.length-1){
                str.append(",");
            }else {
                str.append("}");

            }

        }
return str.toString();
    }
}
