package com.jxj.util;

public class ResponseUtil {
    public static String resp(Integer num,String obj,String msg){
       StringBuffer str=new StringBuffer("{");
        str.append("\"code\":");
        str.append(num);
        str.append(",");

        if (obj !=null) {
            str.append("\"data\":");
            str.append(obj);
            str.append(",");

        }
        str.append("\"msg\":");
        str.append("\""+msg+"\"");
        str.append("}");
        return str.toString();

    }
}
